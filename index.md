## Timeline

- **November/December**: Application will be emailed to all CS/DS via email.
- **Mid-January**: Midshipmen will be paired with internship projects, and midshipmen will be asked to accept internships. After accepting an internship spot, midshipmen must withdraw from all other summer training applications.
- **February-March**: Summer Training blocks will be filled. Midshipmen will be put in contact with summer sponsors.
- **March-April**: We will host an all-intern meeting with faculty where we go over what to expect during the summer and common issues from previous internships.

## Administration and Planning

- Once you accept a summer training option, you are committed to that option. If you select a different summer training option, you must let us (the CS/DS internship team) know so we can fill that spot with another student. 
- You must have a credit card (debit cards don’t count) to execute a summer internship.
- For local internships, you or a partner must have a POV owned by you or a family member available during the summer. (Rising 2/C interns will be able to park on the yard during their internship summer block.)
- Summer internships are available only for rising 1/C and rising 2/C.

## Current Opportunities

Here are the Computer Science and Data Science internships currently planned for next summer.

### Naval Research Laboratory - SIMDIS Data Visualization (Washington, DC)

- The Naval Research Lab (NRL) developed a data visualization tool called SIMDIS that is designed to visualize time-tagged geo data. It is commonly used by test ranges to display live or reconstructed flight test events - showing where aircraft, ships, or ground vehicles were and where their antennas were pointed. NRL has recently completed some field tests that now require deeper data analysis to be able to visualize where all of their sensors and antenna beams were pointing. This internship will work directly with real-world time-tagged geo data (in various formats), develop software to convert the data, and run the SIMDIS tool on it.
- Requirements: Rising 1/C or 2/C in a computer programming based major, CS or DS recommended. Desirable MIDN has scripting knowledge and computer programming skills in multiple languages.
- POC: LT Brett Gentile

### Naval Research Laboratory - Integrations of Goal Reasoning Agents with Generative AI Systems (Washington, DC)

- This internship focuses on exploring naval applications for autonomous goal reasoning (GR) agents integrated with Generative AI systems. Midshipmen will research and identify potential use cases for agents capable of intelligent disobedience, particularly in naval contexts. Working directly with NRL researchers, GenAI experts, and applied scientists across various disciplines, interns will produce a comprehensive project report describing goal reasoning systems, their potential GenAI integrations, recommended naval applications to pursue, and critical research gaps that need to be addressed.
- Requirements: Field of study must be a computer programming-based major. SCS or SDS is recommended, and must be a rising 2/C or 1/C during the anticipated internship. Strong programming ability in Python or related language is recommended. Math and Statistics is helpful.
- POC: LT Brett Gentile

### Naval Meteorology and Oceanography Command - Data Handling Strategies (Stennis Space Center, MS)

- Midshipmen will be part of a transformative shift in data handling strategies at COMNAVMETOCCOM. They will assist in testing and applying new strategies for data processing optimization, collaborate with the Chief Data Officer, review contractor tests, suggest data fabric and cloud architectures, and conduct use-case testing. The goal is to manage the escalating data volumes from new models, which have daily volumes measured in terabytes. Midshipmen will use their programming skills to help implement a new common data fabric and infrastructure across multiple commands that supports both Machine-to-Machine (M2M) and Human-to-Machine (H2M) data dissemination.
- Requirements: Rising 1/C DS major preferred.
- POC: CDR Kenneth Maroon

### Undersea Warfighting Development Center - AI Pipeline Development (Groton, CT)

- Midshipmen will engage in the development of an end-to-end AI/ML pipeline. This experience will offer exposure to team-based engineering, software development, real-world AI and algorithmic research, and insight into problem-definition tasks.
- Requirements: Engineering or Science Major. 1/C Preferred. Python, Java, R, helpful. Experience in AWS cloud helpful
- POC: CDR Kenneth Maroon

### OUSD Research and Engineering - Advanced Military Communications (Little Creek, VA \| Pearl Harbor, HI)

- One project centers around 5G and advanced military communications, cybersecurity, and Defense R&D, focusing on joint and coalition communications between autonomous systems such as unmanned aerial vehicles (UAVs) and Unmanned Surface Vehicles (USVs). These findings will shape the doctrine of utilizing these unmanned vehicles and the communication infrastructure connecting them for a comprehensive common operating picture. Last year's endeavors involved UAVs and USVs in the maritime environment, tapping into augmented reality, STARLINK, and other tools for operations both afloat and ashore. (Little Creek, VA)
- Another project focuses on researching operational use cases linked with swarms of unmanned vehicles, working on the technical intricacies of unmanned C2, and analyzing the cybersecurity and EMCON facets of these networks. Midshipmen are expected to link their insights with operational and technological decision-making. (Pearl Harbor, HI)
- Requirements: CS or DS major with a strong interest in cellular communications and naval special warfare.
- POC: CDR Kenneth Maroon

### Strategic and Tactical Wargaming at Defense Threat Reduction Agency (Maxwell AFB, Alabama)

- This internship will introduce students to military wargaming with an emphasis on designing a wargame involving non-strategic nuclear weapon employment. AF Air University Staff will serve as the primary supervisor, and will guide a joint group of cadets and midshipmen through basic wargame design. The internship will conclude with the students designing and play testing a wargame which they develop. The internship goal is to provide students a basic understanding of military wargaming, so they can apply lessons learned to academic year research projects.
- Requirements: CS or DS major, OR have wargaming experience. Programming experience not required.
- POC: CDR Kenneth Maroon

### NIWC ATLANTIC - Data Science & AI Mission Engineering (Hanahan, SC)

- Midshipmen will work on projects in the Decision Advantage continuum, which includes Mission Engineering, Data Engineering, Data Science/AI, and Battle Management Aid / Visualization (BMA/Viz development). They will have the opportunity to work on operationally focused projects such as Fuels Dashboard/OPREP Improvements, Pierside Maintenance Planning, Maritime Operational Reach Tool (MORT), Task Force 59 / NAVSOUTH, or DoN CDO Data Strike Teams. This internship will provide direct experience in the process of providing leading-edge capabilities to the Navy from design, development, integration, test, and delivery.
- Requirements: Must be a CS, DS, Math, or Engineering major.
- POC: CDR Kenneth Maroon

### Naval Air Warfare Center Training Systems Division - AI for Autonomy in Naval Aviation (Orlando, FL)

- Midshipmen will be part of a team working on solving challenging problems in the utilization of AI for autonomy in Naval aviation. They will use the NERDS in-house high-performance computer, focusing on large scale modeling and simulation to test and validate AI driven autonomous behaviors. The exact project will depend on when the internship occurs, but it will be hands-on and expose the intern to the complexity of utilizing AI at the enterprise level.
- Requirements: CS or DS major. Familiarity with Python is desirable.
- POC: CDR Kenneth Maroon

### Naval Air Warfare Center R&D - Computer Vision Software Development (NAWCAD) (JBMDL, NJ)

- Midshipmen will assist in the development and testing of computer vision software (Python) that tracks objects on the flight deck for carriers. Tasks may include labeling a newly collected dataset, predicting motion and integrating filtering components, fusing estimates from multiple cameras, and creating automatic camera calibration procedures. This internship provides hands-on experience with Python programming, data labeling, and computer vision algorithms, bridging theoretical learning with state-of-the-art applications.
- Requirements: CS or DS major.
- POC: CDR Kenneth Maroon

### General Electric Aerospace - Data Science Solutions (Cincinnati and West Chester, OH)

- This internship at General Electric Aerospace will allow midshipmen to apply data science solutions and production quality code to impact the aviation business. Midshipmen will generate Machine Learning (ML) models for forecasting demand, develop network models, and implement intermediate solutions along the long-term business roadmap. They will see how these models and data impact repair networks and overhaul turnaround times and costs.
- Requirements: CS or DS major. Python or simulation experience is desirable.
- POC: LCDR Justin Downs

### Defense Innovation Unit - Common Operational Database and ARCHER (Mountain View, CA)

- This internship offers two projects: Common Operational Database and ARCHER. The Common Operational Database project involves prototyping a system that incorporates both software and hardware solutions to enhance perception capabilities and create an edge world model suitable for DDIL environments. Midshipmen will work with commercial AI algorithms to analyze data from UxV sensors, focusing on the identification and characterization of marine vessels. They will also work on database management and CDC techniques to streamline data exchange between UxVs and enhance autonomy behaviors. The ARCHER project will develop and deliver capability to provide advantages to Marines operating in the information environment. The solution will leverage and collaborate with tools already in the DoD inventory and prototype additional commercial AI/ML capabilities to improve the military’s ability to maneuver in the information environment.
- Requirements: CS or DS major.
- POC: LCDR Justin Downs

### Chief Digital and Artificial Intelligence Office - Generative AI Exploration (CDAO) Directorate for Algorithmic Warfare (Falls Church, VA)

- This internship will involve the exploration and responsible fielding of generative AI. Midshipmen will work on techniques to identify enterprise services and infrastructure to accelerate AI/machine learning development and adoption. Midshipmen will be exposed to team-based engineering, software development, real-world AI and algorithmic research, and gain experience in problem-definition tasks.
- Requirements: CS or DS Major.
- Highly Desirables: Ability to code in any of the following: Python,  SQL, R, Scala, UDFs, pandas, Java
- Other Desirables: Experience working in databricks and Microsoft Power Apps/Power BI
- POC: CDR Kenneth Maroon

### BlueVoyant Government Solutions - Supply Chain Analysis (Washington, DC)

- Midshipmen will gain hands-on experience in supply chain analysis, data analytics, and risk assessment with BlueVoyant's cybersecurity solutions. They will help DoD customers better understand dependencies within their critical supply chains. Midshipmen will execute identification and mapping of lower-tier suppliers using a blend of open and proprietary data sources and analysis techniques.
- Requirements: CS or DS major. Python and R experience helpful.
- POC: LCDR Justin Downs

### NSWCDD Dam Neck - Machine Learning and Virtualization for Surface Navy Training Systems (Virginia Beach, VA)

- Midshipmen will have the opportunity to work on two exciting projects at NSWCDD Dam Neck Activity. The first project involves the use of Machine Learning, Natural Language Processing, and Virtualization to develop advanced training models and systems for the Surface Navy. Midshipmen will gain insights into the development of the Surface Training Advanced Virtual Environment (STAVE) – a Combat Systems (CS) training capability that uses Virtual Maintenance Trainers (VMT) to provide realistic, cost-effective training for enlisted personnel. The Learning Record Store (LRS) program will also provide administrative support to optimize the use of VMT training class availability. 
- The second project, is focused on the development of a next-generation Radio Frequency (RF) signal collection system. This system uses Machine Learning algorithms to detect and classify received signals across a wide RF spectrum, improving operator effectiveness and efficiency. Midshipmen will be involved in the development, training, and implementation of new RFML models, as well as designing and implementing workflows to allow operators to quickly organize, record, and document new types of signals.
- Requirements: CS or DS major
- POC: CDR Kenneth Maroon

### NSWDG - Advanced Software and Algorithm Development (Virginia Beach, VA)

- Midshipmen will work with NSWDG staff to employ programing, cyber, machine learning, artificial intelligence, and data science skills in the development of applications to support NSWDG mission areas. This internship will allow rising 1/C to get a head start on their Capstone project by building an understanding of the NSWDG mission set and operation parameters. Midshipmen will be expected to apply their creativity and abilities to develop algorithms and software for NSWDG.
- Requirements: Must be a rising 1/C CS/DS/Cyber major. Would prefer rising 1/C to support follow on capstone opportunities.
- POC: CDR Kenneth Maroon

### CRANE / Office of Naval Research - Various Projects (Washington, DC)
- Midshipmen will assist in the development of fully autonomous Low Profile Vessels and develop solutions for a mixed set of objectives. The vessels will participate in the Artificial Intelligence Maritime Maneuver Indiana Collegiate Challenge. This internship provides hands-on experience with autonomous systems and AI in a maritime environment.
- Requirements: Must be CS or DS major. Rising 1/C preferred to support follow-on capstone opportunities.
- POC: CDR Kenneth Maroon

### Lockheed Martin Corporation - Various Projects (Riviera Beach, FL \| Cherry Hill, NJ \| Orlando, FL \| Huntsville, AL \| Englewood, CO)

- ***When applying for these internships, midshipmen will simply apply for the "LMC Internship". After selection, the selectees will then apply for the specific project.***

- This internship is with the defense contractor, Lockheed Martin. There will be several projects available, although the exact projects are still being determined.
- Requirements: Rising 1/C in CS or DS major. Applicants must have strong programming skills. AI, ML, and HPC skills are recommended. Applicants should have necessary coursework to contribute to the project.
- POC: Prof. Seung Geol Choi

### Draper Laboratory - Autonomous System Design & Analysis (Cambridge, Massachusetts)

- ***This internship will be applied for separately from the other CS/DS internships. The application will be sent out via email.***

- Midshipmen will be involved in solving challenging, cutting-edge problems in areas such as Autonomous System Design & Analysis, Advanced Autonomy, Navigation, & Machine Learning, Intelligence Algorithms, Search Programs, & Modeling/Simulation Techniques, and more. Past projects have included underwater implosions, anomaly detection methods for UUVs, UUV sonar system integration, robust mission planning/navigation for UUVs, and autonomous control software development for UUVs.
- Requirements: Must be a rising 2/C or 1/C midshipman majoring in a STEM field (M&S Division or E&W Division).
- POC: CDR Edgar Jatho